###Team 1: Momentum###
David Chen, Karl Xu, Elizabeth Caronia

###Team 2: CSD#
David Axelrod, Celina Kilcrease, Shubhra Para

###Team 3: Apollo#
Ketan Reddy, Robert Trujillo, Zoe Grippo

###Team 4: Swift#
Chris Luo, Jessica Zhang, Manan Singh

###Team 5: Goldfish#
Josh Keough, Charles Pigott, Kyle Gruber

###Team 6: pseudo code (C#)#
Kiran Bhimani, Farwa Abbas, Kimberly Allan, Jaspareet Nahal, Philip Mak

###Team 7: JAVET#
Abnob Doss, Julie Rosenbaum, Vicki Lee

###Team 8: Team 8#
Alex Bae, Chris Grasing, Raisa Rabbani

###Team 9: Production Support Baes#
Haley Simmons, Matias Castro, Ian Jack, Mackenzie Benton, Rebecca Ho
